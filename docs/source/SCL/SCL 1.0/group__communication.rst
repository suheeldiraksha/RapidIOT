======================
SCL communication API
======================

.. doxygengroup:: communication
   :project: SCL 1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
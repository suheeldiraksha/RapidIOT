==========
Variables
==========

   .. container:: contents

       
      .. rubric:: - a -
         :name: a--

      -  activity_cb :
         `SCL_EMAC <class_s_c_l___e_m_a_c.html#a061455b666c7db8c59bf8b536a9b1ac8>`__
      -  assoc :
         `scl_listen_interval_t <structscl__listen__interval__t.html#af001610277052191979db17c0133c933>`__
      -  atim_window :
         `wl_bss_info_struct <structwl__bss__info__struct.html#a1bec52968a72b2101def88458f0371ec>`__

      .. rubric:: - b -
         :name: b--

      -  band :
         `scl_scan_result <structscl__scan__result.html#ad9563fe35a27d0b365b73adc0b871714>`__
      -  basic_mcs :
         `wl_bss_info_struct <structwl__bss__info__struct.html#a7efd75a130623350ac4a98d3688ee791>`__
      -  beacon :
         `scl_listen_interval_t <structscl__listen__interval__t.html#a59de6cdff8214507260f142834f20cee>`__
      -  beacon_period :
         `wl_bss_info_struct <structwl__bss__info__struct.html#ab3cd1ed6ac499c1d63772712337edba8>`__
      -  bss_type :
         `scl_scan_result <structscl__scan__result.html#a098f4e257c2ff5999c247cb2cb2530d2>`__
      -  BSSID :
         `scl_scan_result <structscl__scan__result.html#a7a56c5ec5993ee1d118e3ffe94b2151e>`__
         ,
         `scl_simple_scan_result <structscl__simple__scan__result.html#a7a56c5ec5993ee1d118e3ffe94b2151e>`__
         ,
         `wl_bss_info_struct <structwl__bss__info__struct.html#a7a56c5ec5993ee1d118e3ffe94b2151e>`__
      -  buffer :
         `scl_tx_buf <group__wifi.html#ga6164f96b054c947752bce394af84b548>`__

      .. rubric:: - c -
         :name: c--

      -  capability :
         `wl_bss_info_struct <structwl__bss__info__struct.html#a4d6a7cc78819fe901fa45a1b51266e18>`__
      -  ccode :
         `scl_scan_result <structscl__scan__result.html#a4f626e97f0f0ea28fea702a7ecaad54f>`__
      -  channel :
         `scl_scan_result <structscl__scan__result.html#a715f5cb061d11eb75981741eda4dafcd>`__
         ,
         `scl_simple_scan_result <structscl__simple__scan__result.html#a715f5cb061d11eb75981741eda4dafcd>`__
      -  chanspec :
         `wl_bss_info_struct <structwl__bss__info__struct.html#aa5d9886a989c0e36349581e3f06cd4c0>`__
      -  connection_status :
         `network_params <group__communication.html#ga5ab54995772ff96d303a45287001664f>`__
      -  count :
         `wl_bss_info_struct <structwl__bss__info__struct.html#a86988a65e0d3ece7990c032c159786d6>`__
      -  ctl_ch :
         `wl_bss_info_struct <structwl__bss__info__struct.html#aaba7272626229ace409b150bb1981044>`__

      .. rubric:: - d -
         :name: d--

      -  dtim :
         `scl_listen_interval_t <structscl__listen__interval__t.html#a011e406e7bba2200194b90d308ea2e82>`__
      -  dtim_period :
         `wl_bss_info_struct <structwl__bss__info__struct.html#a2817cb129e69f6ce5a24dfb2dd8706cc>`__

      .. rubric:: - e -
         :name: e--

      -  emac_link_input_cb :
         `SCL_EMAC <class_s_c_l___e_m_a_c.html#a20c74fe6971854f0c192f379eef5bd7d>`__
      -  emac_link_state_cb :
         `SCL_EMAC <class_s_c_l___e_m_a_c.html#a08b48e3719eb5fffedfa9bfadd396135>`__

      .. rubric:: - f -
         :name: f--

      -  flags :
         `scl_scan_result <structscl__scan__result.html#aa2585d779da0ab21273a8d92de9a0ebe>`__
         ,
         `wl_bss_info_struct <structwl__bss__info__struct.html#aa2585d779da0ab21273a8d92de9a0ebe>`__

      .. rubric:: - g -
         :name: g--

      -  gateway :
         `network_params <group__communication.html#ga3cd34dd09ff5b61fd63a489a7cf8ae7c>`__

      .. rubric:: - i -
         :name: i--

      -  ie_len :
         `scl_scan_result <structscl__scan__result.html#a5530db0834a2796234ed42c3fcb5c0f4>`__
      -  ie_length :
         `wl_bss_info_struct <structwl__bss__info__struct.html#a5312679515b5ee9c1875196c703a8a52>`__
      -  ie_offset :
         `wl_bss_info_struct <structwl__bss__info__struct.html#a67b93773e8da423543b703cad84da5a5>`__
      -  ie_ptr :
         `scl_scan_result <structscl__scan__result.html#a460ec020636ddecd8758543cfa83e101>`__
      -  interface_type :
         `SCL_EMAC <class_s_c_l___e_m_a_c.html#a4ebd989c528bc76d7f448c0e97349b51>`__
      -  ip_address :
         `network_params <group__communication.html#gac5432aed416c5c95736350828bb7b64d>`__

      .. rubric:: - l -
         :name: l--

      -  length :
         `scl_ssid_t <structscl__ssid__t.html#ab2b3adeb2a67e656ff030b56727fd0ac>`__
         ,
         `wl_bss_info_struct <structwl__bss__info__struct.html#aebb70c2aab3407a9f05334c47131a43b>`__
      -  link_state :
         `SCL_EMAC <class_s_c_l___e_m_a_c.html#a283b2d2eb956854aedeb4000299094e2>`__

      .. rubric:: - m -
         :name: m--

      -  max_data_rate :
         `scl_scan_result <structscl__scan__result.html#a2deb22a1108e6c9371d92d496c07da01>`__
      -  memory_manager :
         `SCL_EMAC <class_s_c_l___e_m_a_c.html#a994a6cb529b7d4982e157bd0c52a6faa>`__
      -  multicast_addr :
         `SCL_EMAC <class_s_c_l___e_m_a_c.html#ae708321aec466fb83949a1ac496bfde7>`__

      .. rubric:: - n -
         :name: n--

      -  n_cap :
         `wl_bss_info_struct <structwl__bss__info__struct.html#aaa58323f8c11ee56cc8d8c2e66938230>`__
      -  nbss_cap :
         `wl_bss_info_struct <structwl__bss__info__struct.html#aafd41688d55159e60c6970dddbfdf4ba>`__
      -  netmask :
         `network_params <group__communication.html#gabe8b36c35606ee162f7e876cd4d93b7d>`__
      -  next :
         `scl_scan_result <structscl__scan__result.html#a3b12147732965a4802397fe97eaf1ef6>`__
      -  number_of_probes_per_channel :
         `scl_scan_extended_params_t <structscl__scan__extended__params__t.html#ae142c15dcc2633a4ab09a282fc942186>`__

      .. rubric:: - o -
         :name: o--

      -  octet :
         `scl_mac_t <structscl__mac__t.html#abc3755f1f66dea95fce153ee4f49e907>`__

      .. rubric:: - p -
         :name: p--

      -  phy_noise :
         `wl_bss_info_struct <structwl__bss__info__struct.html#a85ba569dec5085a93016434929bbf7d1>`__
      -  powered_up :
         `SCL_EMAC <class_s_c_l___e_m_a_c.html#a0855c633d232d16b032babd0bb164930>`__

      .. rubric:: - r -
         :name: r--

      -  rates :
         `wl_bss_info_struct <structwl__bss__info__struct.html#a686551a9eef0a3911a371e315bb24d36>`__
      -  rateset :
         `wl_bss_info_struct <structwl__bss__info__struct.html#a9efbba9294bd0e37a9557dceed177ff2>`__
      -  reserved :
         `wl_bss_info_struct <structwl__bss__info__struct.html#a72aca6ea6d8153b28ea8f139b932ec3e>`__
      -  reserved32 :
         `wl_bss_info_struct <structwl__bss__info__struct.html#ae3f4b5f7822024fbf0bea3a8845c67c4>`__
      -  RSSI :
         `wl_bss_info_struct <structwl__bss__info__struct.html#ae45c71dee229890b4401a4c0105d73cf>`__

      .. rubric:: - s -
         :name: s--

      -  scan_active_dwell_time_per_channel_ms :
         `scl_scan_extended_params_t <structscl__scan__extended__params__t.html#addbd186dd08b75e5de298a3d8dbb76a1>`__
      -  scan_home_channel_dwell_time_between_channels_ms :
         `scl_scan_extended_params_t <structscl__scan__extended__params__t.html#a325bae58ff955b428047edab0a2f0799>`__
      -  scan_passive_dwell_time_per_channel_ms :
         `scl_scan_extended_params_t <structscl__scan__extended__params__t.html#afe48c256e4f4f14eb202d51dd8348818>`__
      -  security :
         `scl_scan_result <structscl__scan__result.html#a8410644c7ed11ab7094fa5328612de73>`__
         ,
         `scl_simple_scan_result <structscl__simple__scan__result.html#a8410644c7ed11ab7094fa5328612de73>`__
      -  signal_strength :
         `scl_scan_result <structscl__scan__result.html#ac303b69da3c469c92299a6ff260e2859>`__
         ,
         `scl_simple_scan_result <structscl__simple__scan__result.html#ac303b69da3c469c92299a6ff260e2859>`__
      -  size :
         `scl_tx_buf <group__wifi.html#gab2c6b258f02add8fdf4cfc7c371dd772>`__
      -  SNR :
         `wl_bss_info_struct <structwl__bss__info__struct.html#abcbf8106d506ba6a33c01411d5ec3e99>`__
      -  SSID :
         `scl_scan_result <structscl__scan__result.html#a2d48b9ce5c7b331ba0eb3d0085048b23>`__
         ,
         `scl_simple_scan_result <structscl__simple__scan__result.html#a2d48b9ce5c7b331ba0eb3d0085048b23>`__
         ,
         `wl_bss_info_struct <structwl__bss__info__struct.html#a337a4a90b9c8cb320d1232cf9f88fa90>`__
      -  SSID_len :
         `wl_bss_info_struct <structwl__bss__info__struct.html#a7eafb3bd3419add91d9d532df5dcf63e>`__

      .. rubric:: - v -
         :name: v--

      -  value :
         `scl_ssid_t <structscl__ssid__t.html#aa88a4115b417ed84082f85ab347f4b02>`__
      -  version :
         `wl_bss_info_struct <structwl__bss__info__struct.html#acd99bb05ca015e7d74448acb1deba7ca>`__

.. container:: navpath
   :name: nav-path

   -  Copyright Cypress Corporation.

.. |Cypress Logo| image:: cypress_logo.png

======================================
scl_interface_shared_info_t
======================================

.. doxygenstruct:: scl_interface_shared_info_t
   :project: SCL 1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members:

==========
Functions
==========

   .. container:: contents

       
      .. rubric:: - a -
         :name: a--

      -  add_multicast_group() :
         `SCL_EMAC <class_s_c_l___e_m_a_c.html#a340c2d7245ac887e218a2c8d10a60eca>`__

      .. rubric:: - c -
         :name: c--

      -  connect() :
         `SclSTAInterface <class_scl_s_t_a_interface.html#a8e2de81690a85a06cb3911fe4d7684ad>`__

      .. rubric:: - d -
         :name: d--

      -  disconnect() :
         `SclSTAInterface <class_scl_s_t_a_interface.html#a59877b997f79764cc2dcc00dbdade6f0>`__

      .. rubric:: - g -
         :name: g--

      -  get_align_preference() :
         `SCL_EMAC <class_s_c_l___e_m_a_c.html#a8609f29d6eea58f1d9b33755b2a2a92b>`__
      -  get_bss_type() :
         `SclAccessPoint <class_scl_access_point.html#aee90cd855230ed2760ead73b547386db>`__
      -  get_bssid() :
         `SclSTAInterface <class_scl_s_t_a_interface.html#a8a47f3bdf1bfc93c7cb3926540c0985e>`__
      -  get_default_instance() :
         `SclSTAInterface <class_scl_s_t_a_interface.html#adc18cebd20bf502e85b13f0116aeb4c7>`__
      -  get_hwaddr() :
         `SCL_EMAC <class_s_c_l___e_m_a_c.html#a44bce8dc45c6e4ceed7e0e83c0f75aad>`__
      -  get_hwaddr_size() :
         `SCL_EMAC <class_s_c_l___e_m_a_c.html#ab9a4e2d7d9946417be8e061b22345076>`__
      -  get_ie_data() :
         `SclAccessPoint <class_scl_access_point.html#a37ea3ec3255e78bdb2c07411088be185>`__
      -  get_ie_len() :
         `SclAccessPoint <class_scl_access_point.html#abee7f14c1fdcf9b0c9754e22a263f6b9>`__
      -  get_ifname() :
         `SCL_EMAC <class_s_c_l___e_m_a_c.html#ae45ab3743bcb4f07efc804436e8ba1fe>`__
      -  get_instance() :
         `SCL_EMAC <class_s_c_l___e_m_a_c.html#ad6d7c72b2a0bdcd06d63a85b05c0e919>`__
      -  get_mtu_size() :
         `SCL_EMAC <class_s_c_l___e_m_a_c.html#a5f94a897052d19ee78f810f6e1c34f1f>`__
      -  get_rssi() :
         `SclSTAInterface <class_scl_s_t_a_interface.html#a5c0307261cef92c1205c8bb00104b0de>`__

      .. rubric:: - i -
         :name: i--

      -  is_interface_connected() :
         `SclSTAInterface <class_scl_s_t_a_interface.html#a8e860b750bde14c86224cb128f3d9e62>`__

      .. rubric:: - l -
         :name: l--

      -  link_out() :
         `SCL_EMAC <class_s_c_l___e_m_a_c.html#a5df8125e5c61ed3900d72b4a522f72bb>`__

      .. rubric:: - o -
         :name: o--

      -  operator=() :
         `SclAccessPoint <class_scl_access_point.html#ab907ee6895c137e2c52b09ca4f02d0c4>`__

      .. rubric:: - p -
         :name: p--

      -  power_down() :
         `SCL_EMAC <class_s_c_l___e_m_a_c.html#ab8bc9384f2d57b21421cf71466aa8006>`__
      -  power_up() :
         `SCL_EMAC <class_s_c_l___e_m_a_c.html#adf301402150f55f81f030f9ba881aad2>`__

      .. rubric:: - r -
         :name: r--

      -  remove_multicast_group() :
         `SCL_EMAC <class_s_c_l___e_m_a_c.html#a06d366e890de9ce03ba21c8500fd857c>`__

      .. rubric:: - s -
         :name: s--

      -  scan() :
         `SclSTAInterface <class_scl_s_t_a_interface.html#a86ac41e2aaacd5c4716a66b5547deabe>`__
      -  set_activity_cb() :
         `SCL_EMAC <class_s_c_l___e_m_a_c.html#a87faa7f48dbc90b233d63f527c153bc2>`__
      -  set_all_multicast() :
         `SCL_EMAC <class_s_c_l___e_m_a_c.html#a4e0c501a446dd6c54d5a77904104afdf>`__
      -  set_blocking() :
         `SclSTAInterface <class_scl_s_t_a_interface.html#a998aeecd5b55496f48c928ccdefdc987>`__
      -  set_channel() :
         `SclSTAInterface <class_scl_s_t_a_interface.html#a2f235156b6a66e26795d3aa44e37fa1c>`__
      -  set_credentials() :
         `SclSTAInterface <class_scl_s_t_a_interface.html#af5773297b57dd0a4f923c4162d629994>`__
      -  set_hwaddr() :
         `SCL_EMAC <class_s_c_l___e_m_a_c.html#a28178b0b5535b3204cb470e06c5e8179>`__
      -  set_link_input_cb() :
         `SCL_EMAC <class_s_c_l___e_m_a_c.html#a4f3c93d2eddbfaa7377357794eacc542>`__
      -  set_link_state_cb() :
         `SCL_EMAC <class_s_c_l___e_m_a_c.html#a055c7d5eb741e12b6f5c9b43158214d7>`__
      -  set_memory_manager() :
         `SCL_EMAC <class_s_c_l___e_m_a_c.html#ae71a3afbcfd9ef9391a67ea8c116f7ee>`__

      .. rubric:: - w -
         :name: w--

      -  wifi_on() :
         `SclSTAInterface <class_scl_s_t_a_interface.html#a7ff2ace1e4e2de2e56f0ac98adda44fe>`__
      -  wifi_set_up() :
         `SclSTAInterface <class_scl_s_t_a_interface.html#a46917d65aeb2ea44c48e29cd1b004a83>`__



==========
Files List
==========

.. container:: header

      .. container:: headertitle

   .. container:: contents

      .. container:: textblock

         Here is a list of all documented files with brief descriptions:

      .. container:: directory

         +---------------------------------------------------------------------+--------------------------------------------------------------------+
         |  \ `scl_buffer_api.h <scl__buffer__api_8h.html>`__                  | Provides declarations for buffer management functionality          |
         +---------------------------------------------------------------------+--------------------------------------------------------------------+
         |  \ `scl_common.h <scl__common_8h.html>`__                           | Defines common data types used in SCL                              |
         +---------------------------------------------------------------------+--------------------------------------------------------------------+
         |  \ `scl_emac.h <scl__emac_8h.html>`__                               | Provides EMAC interface functions to be used with the              |
         |                                                                     | `SCL_EMAC <class_s_c_l___e_m_a_c.html>`__ object                   |
         +---------------------------------------------------------------------+--------------------------------------------------------------------+
         |  \ `scl_ipc.h <scl__ipc_8h.html>`__                                 | Provides SCL functionality to communicate with Network Processor   |
         +---------------------------------------------------------------------+--------------------------------------------------------------------+
         |  \ `scl_types.h <scl__types_8h.html>`__                             | Defines common data types used in SCL                              |
         +---------------------------------------------------------------------+--------------------------------------------------------------------+
         |  \ `scl_version.h <scl__version_8h.html>`__                         | Provides version number of SCL                                     |
         +---------------------------------------------------------------------+--------------------------------------------------------------------+
         |  \ `scl_wifi_api.h <scl__wifi__api_8h.html>`__                      | Prototypes of functions for controlling the Wi-Fi system           |
         +---------------------------------------------------------------------+--------------------------------------------------------------------+
         |  \ `SclSTAInterface.h <_scl_s_t_a_interface_8h.html>`__             | Provides SCL interface functions to be used with WiFiInterface or  |
         |								       | NetworkInterface Objects					    |
         +---------------------------------------------------------------------+--------------------------------------------------------------------+

   

.. toctree::
   :maxdepth: 8
   :hidden:
   
   scl__buffer__api_8h.rst
   scl__common_8h.rst
   scl__emac_8h.rst
   scl__ipc_8h.rst
   scl__types_8h.rst
   scl__version_8h.rst
   scl__wifi__api_8h.rst
   _scl_s_t_a_interface_8h.rst



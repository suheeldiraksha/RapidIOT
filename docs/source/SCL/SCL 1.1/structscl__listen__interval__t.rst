======================
scl_listen_interval_t 
======================

.. doxygenstruct:: scl_listen_interval_t
   :project: SCL 1.1
   :members:
   :protected-members:
   :private-members:
   :undoc-members:

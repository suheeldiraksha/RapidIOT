======================
SCL communication API
======================

.. doxygengroup:: communication
   :project: SCL 1.1
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
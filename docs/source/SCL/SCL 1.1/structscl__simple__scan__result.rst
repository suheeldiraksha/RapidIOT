=======================
scl_simple_scan_result 
=======================

.. doxygenstruct:: scl_simple_scan_result
   :project: SCL 1.1
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
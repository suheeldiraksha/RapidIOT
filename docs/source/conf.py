# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
sys.path.insert(0, os.path.abspath('.'))
import xml.etree.ElementTree as ET
from os import path


# -- Project information -----------------------------------------------------

project = 'RapidIOT'
copyright = '2020, Infineon'
author = 'Infineon'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = ['breathe' ]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_rtd_theme'

html_theme_options = {
    'display_version': False,
    'prev_next_buttons_location': 'bottom',
    # Toc options
    'collapse_navigation': True,
    'sticky_navigation': True,
    'navigation_depth': 6,
    'includehidden': True,
    'titles_only': False
}

breathe_projects = {'SCL 1.0' : 'SCL/SCL 1.0/xml/','SCL 1.1' : 'SCL/SCL 1.1/xml/'}

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']


html_logo = '_static/image/IFX_LOGO_RGB.svg'

# The name of an image file (within the static path) to use as favicon of the
# docs.  This file should be a Windows icon file (.ico) being 16x16 or 32x32
# pixels large.
html_favicon = '_static/image/IFX_ICON.ico'

def setup(app):
#    app.add_css_file('css/custom.css')
    app.add_css_file('css/theme_overrides.css')
    app.add_js_file('js/custom.js')
    app.add_js_file('js/searchtools.js')
    app.add_js_file('js/hotjar.js')
    app.add_js_file('js/sajari.js')

for (k, filePath) in breathe_projects.items():
  files = os.listdir(filePath)
  for nameOfFile in files:
    if nameOfFile.endswith(".xml"):
#      print(filePath)
      fileName=filePath+nameOfFile
      tempfilename=filePath+'tempFileName.xml'
      print(fileName)
      print(path.exists(fileName))
      ulinkArray=[]
      titleArray=[]
      directiveList=[]
      totalDirectiveList=['seealso', 'attention', 'caution', 'danger', 'error', 'hint', 'important', 'note', 'tip', 'warning', 'admonition']
      if path.exists(fileName):
        with open(fileName, encoding='UTF-8') as f:
          tree = ET.parse(f)
          root = tree.getroot()
#          print(ET.tostring(root, encoding='utf8').decode('utf8'))
          for elem in root.getiterator():
            if (elem.tag == 'compounddef' and elem.attrib['kind']!=None and elem.attrib['kind'] == 'page'):
              elem.attrib['kind'] = 'group'
            try:
              if elem.tag == 'title':
                text = elem.text
                titleArray.append(str(text))

              if elem.tag == 'image':
                if elem.attrib['type']!= None and elem.attrib['type'] == 'latex':
                  elem.attrib.pop('type')
                  elem.attrib.pop('name')
                  elem.tag = elem.tag.replace(elem.tag,'removeimage') 

              if 'ulink' == elem.tag: 
                if elem.attrib['url']!= None:
#                  print(elem.attrib['url'])  
                  ulinkArray.append('<ulink url="'+elem.attrib["url"]+'">')

              if ("sect" in elem.tag and elem.tag!='simplesect' and elem.tag!='sectiondef'): 
                if elem.attrib['id']!= None:
                  elem.attrib.pop('id')
                  elem.tag = elem.tag.replace(elem.tag,'removesec')

              if ( elem.tag == 'simplesect' and elem.attrib['kind']!=None and elem.attrib['kind'] in totalDirectiveList ):
                directiveList.append(elem.attrib['kind'])
                elem.attrib.pop('kind')
                elem.tag = elem.tag.replace(elem.tag,'simplesectreplace') 

            except AttributeError:
              pass
              
#          print("before write")
          tree.write(tempfilename, encoding="utf-8")

        roottext=''    
        with open(tempfilename, encoding='UTF-8') as file:
          tree = ET.parse(file)
          root = tree.getroot()
          xmlstr = ET.tostring(root, encoding='utf8').decode('utf8')
          roottext = xmlstr.replace('<removesec>','').replace('</removesec>','').replace('None','')
          roottext = roottext.replace('<removeimage>','').replace('</removeimage>','').replace('<removeimage />','').replace('<removeimage/>','')
          for ulink in ulinkArray: 
            roottext = roottext.replace(ulink+"<bold>",ulink)

          roottext = roottext.replace('</bold></ulink>','</ulink>')
          
          for titleText in titleArray: 
            underline=''
            for charec in titleText:
              underline=underline+'-'
        
            repContent='<title>'+titleText+'</title>'
            repToContent='<para><verbatim>embed:rst \n'+titleText+'\n'+underline+'\n</verbatim></para>'
            roottext = roottext.replace(repContent,repToContent)
          
          for dirName in directiveList:
            roottext = roottext.replace('<simplesectreplace><para>','<para><verbatim>embed:rst \n.. '+dirName+'::\n   ').replace('</para></simplesectreplace>','\n</verbatim></para>')
            
#        print(roottext)
        
        tree = ET.ElementTree(ET.fromstring(roottext))
        tree.write(fileName, encoding="utf-8")
        os.remove(tempfilename)
        
      print("END Loop")
      
